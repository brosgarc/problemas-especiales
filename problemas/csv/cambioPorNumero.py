# -*- coding: utf-8 -*-
import csv
from unidecode import unidecode




reader = csv.reader(open('dataset.csv', 'rb'))
for index,row in enumerate(reader):
	csvFile = open('dataset_sin_tilde.csv', 'a')
	csvWriter = csv.writer(csvFile)
	facultad=0
	if row[3]=='DIRECCIONDETECNOLOGIAS':
		facultad=1
	elif row[3]=='CELEX':
		facultad=2
	elif row[3]=='EscueladeDiseñoyComunicaciónVisual':
		facultad=3
	elif row[3]=='FacultaddeIngenieríaenCienciasdelaTierra':
		facultad=4
	elif row[3]=='FacultaddeIngenieríaenElectricidadyComputación':
		facultad=5
	elif row[3]=='FacultaddeIngenieríaenMecánicayCienciasdelaProducción':
		facultad=6
	elif row[3]=='FacultaddeIngenieríaMarítima,CienciasBiológicas,OceánicasyRecursosNaturales':
		facultad=7
	elif row[3]=='InstitutodeCienciasFísicas':
		facultad=8
	elif row[3]=='FacultaddeCienciasSocialesyHumanísticas':
		facultad=9
	elif row[3]=='InstitutodeCienciasMatemáticas':
		facultad=10
	elif row[3]=='InstitutodeCienciasQuímicasYAmbientales':
		facultad=11
	elif row[3]=='ProgramadeTecnologíaAgrícola':
		facultad=12
	elif row[3]=='ProgramadeTecnologíaenAlimentos':
		facultad=13
	elif row[3]=='ProgramadeTecnologíaenComputación':
		facultad=14
	elif row[3]=='ProgramadeTecnologíaEléctricayElectrónica':
		facultad=15
	elif row[3]=='ProgramadeTecnologíaMecánica':
		facultad=16
	elif row[3]=='ProgramadeTecnologíaPesquera':
		facultad=17
	elif row[3]=='FacultaddeCienciasNaturalesyMatemáticas':
		facultad=18
	elif row[3]=='99':
		facultad=19
	try:
		csvWriter.writerow([row[0],row[1],row[2],facultad])
	except:
		pass