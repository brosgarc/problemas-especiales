from dataset.models import *
from django.contrib import admin

admin.site.register(Facultad)
admin.site.register(Usuario)
admin.site.register(RelacionUser)
admin.site.register(MentionUser)
admin.site.register(Tweet)