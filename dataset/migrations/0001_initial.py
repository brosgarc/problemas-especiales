# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Facultad',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=5050, verbose_name=b'Nombre')),
                ('siglas', models.CharField(max_length=50, verbose_name=b'Siglas')),
            ],
        ),
        migrations.CreateModel(
            name='MentionUser',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('id_usuario_1', models.IntegerField(verbose_name=b'Usuario1')),
                ('id_usuario_2', models.IntegerField(verbose_name=b'Usuario2')),
            ],
        ),
        migrations.CreateModel(
            name='RelacionUser',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('id_usuario_1', models.IntegerField(verbose_name=b'Usuario1')),
                ('id_usuario_2', models.IntegerField(verbose_name=b'Usuario2')),
            ],
        ),
        migrations.CreateModel(
            name='Tweet',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('fecha', models.DateTimeField()),
                ('comentario', models.CharField(max_length=5000, verbose_name=b'Comentario')),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=150, verbose_name=b'Nombre')),
                ('usuario', models.CharField(max_length=150, verbose_name=b'Usuario')),
                ('cant_seguidores', models.IntegerField(verbose_name=b'Seguidores')),
                ('facultad', models.ForeignKey(to='dataset.Facultad')),
            ],
        ),
        migrations.AddField(
            model_name='tweet',
            name='usuario',
            field=models.ForeignKey(to='dataset.Usuario'),
        ),
    ]
