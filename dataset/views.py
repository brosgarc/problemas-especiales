# -*- coding: utf-8 -*-
from django.shortcuts import render
import requests
from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
import csv
from django.conf import settings
from dataset.models import *
import datetime
from unidecode import unidecode
import numpy
# Create your views here.
def insertFacultades(request):
	facultades=['DIRECCION DE TECNOLOGIAS','CELEX','Escuela de Diseño y ComunicaciónVisual','Facultad de Ingeniería en Ciencias de la Tierra','Facultad de Ingeniería en Electricidad y Computación','Facultad de Ingeniería en Mecánica y Ciencias de la Producción','Facultad de Ingeniería Marítima,Ciencias Biológicas,Oceánicas y Recursos Naturales','Instituto de Ciencias Físicas','Facultad de Ciencias Sociales y Humanísticas','Instituto de Ciencias Matemáticas','Instituto de Ciencias Químicas Y Ambientales','ProgramadeTecnologíaAgrícola','Programa de Tecnología en Alimentos','Programa de Tecnología en Computación','Programa de Tecnología Eléctrica y Electrónica','Programa de Tecnología Mecánica','Programa de Tecnología Pesquera','Facultad de Ciencias Naturales y Matemáticas']
	
	for facultad in ['DIRECCION DE TECNOLOGIAS','CELEX','Escuela de Diseño y ComunicaciónVisual','Facultad de Ingeniería en Ciencias de la Tierra','Facultad de Ingeniería en Electricidad y Computación','Facultad de Ingeniería en Mecánica y Ciencias de la Producción','Facultad de Ingeniería Marítima,Ciencias Biológicas,Oceánicas y Recursos Naturales','Instituto de Ciencias Físicas','Facultad de Ciencias Sociales y Humanísticas','Instituto de Ciencias Matemáticas','Instituto de Ciencias Químicas Y Ambientales','ProgramadeTecnologíaAgrícola','Programa de Tecnología en Alimentos','Programa de Tecnología en Computación','Programa de Tecnología Eléctrica y Electrónica','Programa de Tecnología Mecánica','Programa de Tecnología Pesquera','Facultad de Ciencias Naturales y Matemáticas']:
		#print facultad
		obj_facultad=Facultad.objects.create(nombre=facultad,siglas='-')
    	#obj_facultad.save()

def insertUsuarios(request):
	print settings.CSV_FILES
	reader = csv.reader(open(settings.CSV_FILES+'/dataset.csv', 'rb'))
	for index,row in enumerate(reader):
		#try:
		if int(row[3])==1:
			facultad='DIRECCION DE TECNOLOGIAS'
		elif int(row[3])==2:
			facultad='CELEX'
		elif int(row[3])==3:
			facultad='Escuela de Diseño y ComunicaciónVisual'
		elif int(row[3])==4:
			facultad='Facultad de Ingeniería en Ciencias de la Tierra'
		elif int(row[3])==5:
			facultad='Facultad de Ingeniería en Electricidad y Computación'
		elif int(row[3])==6:
			facultad='Facultad de Ingeniería en Mecánica y Ciencias de la Producción'
		elif int(row[3])==7:
			facultad='Facultad de Ingeniería Marítima,Ciencias Biológicas,Oceánicas y Recursos Naturales'
		elif int(row[3])==8:
			facultad='Instituto de Ciencias Físicas'
		elif int(row[3])==9:
			facultad='Facultad de Ciencias Sociales y Humanísticas'
		elif int(row[3])==10:
			facultad='Instituto de Ciencias Matemáticas'
		elif int(row[3])==11:
			facultad='Instituto de Ciencias Químicas Y Ambientales'
		elif int(row[3])==12:
			facultad='ProgramadeTecnologíaAgrícola'
		elif int(row[3])==13:
			facultad='Programa de Tecnología en Alimentos'
		elif int(row[3])==14:
			facultad='Programa de Tecnología en Computación'
		elif int(row[3])==15:
			facultad='Programa de Tecnología Eléctrica y Electrónica'
		elif int(row[3])==16:
			facultad='Programa de Tecnología Mecánica'
		elif int(row[3])==17:
			facultad='Programa de Tecnología Pesquera'
		elif int(row[3])==18:
			facultad='Facultad de Ciencias Naturales y Matemáticas'
		elif int(row[3])==19:
			facultad='Sin Facultad'
		try:
			obj_facultad=Facultad.objects.get(nombre=facultad)
			obj_usuario=Usuario(nombre=row[1]+' '+row[2],usuario=row[0],facultad=obj_facultad,cant_seguidores=0)
			obj_usuario.save()
			print 'usuario guardado'
		except:
			pass
		#except:
		#	print str(row[0])+' no se guardo'

def deleteDobles(request):
	estudiantes = Usuario.objects.all()
	for e in estudiantes:
		try:
			a=Usuario.objects.get(usuario=e.usuario)
		except:
			a=Usuario.objects.filter(usuario=e.usuario)
			for d in a[1:len(a)]:
				d.delete()

def insertCantSeguidores(request):
	reader = csv.reader(open(settings.CSV_FILES+'/datasetSeguidores.csv', 'rb'))
	for index,row in enumerate(reader):
		try:
			u=Usuario.objects.get(usuario=row[0])
			u.cant_seguidores=int(row[1])
			u.save()
		except:
			print 'no existe usuario'
	print len(Usuario.objects.filter(cant_seguidores=0))

def insertMention(request):
	reader = csv.reader(open(settings.CSV_FILES+'/datasetMention.csv', 'rb'))
	csvFile = open(settings.CSV_FILES+'datasetRelaciones.csv', 'a')
	csvWriter = csv.writer(csvFile)
	for index,row in enumerate(reader):
		fecha_hora=str(row[3]).split(' ')
		fecha=fecha_hora[0]
		hora=fecha_hora[1]
		fecha=str(fecha).split('-')
		horam=str(hora).split(':')
		anio=fecha[0]
		mes=fecha[1]
		dia=fecha[2]
		hora=horam[0]
		minuto=horam[1]
		segundo=horam[2]
		
		try:
			try:
				user_1=Usuario.objects.get(usuario=row[0])
				user_2=Usuario.objects.get(usuario=row[1])
				user_m=MentionUser(id_usuario_1=user_1.id,id_usuario_2=user_2.id)
				user_m.save()
			except:
				print 'algun usuario no existe'
			try:
				ut=Usuario.objects.get(usuario=row[1])
				tw=Tweet(usuario=ut,fecha=datetime.datetime(int(anio),int(mes),int(dia),int(hora),int(minuto),int(segundo)),comentario=row[4])
				tw.save()
			except:
				print 'algo paso con el tweet'
			if str(anio)==str(2015):
				csvWriter.writerow([user_1.id,user_2.id,mes])
			else:
				pass
		except:
			pass

def deleteDobleMention(request):
	list_mention=MentionUser.objects.all()
	for m in list_mention:

		if len(MentionUser.objects.filter(id_usuario_1=m.id_usuario_1,id_usuario_2=m.id_usuario_2))>1:
			lm=MentionUser.objects.filter(id_usuario_1=m.id_usuario_1,id_usuario_2=m.id_usuario_2)
			for me in lm[1:len(lm)]:
				me.delete()
		else:
			print 'ok'

def generarUsuariosCSV(request):
	csvFile = open(settings.CSV_FILES+'datasetUsuarios.csv', 'a')
	csvWriter = csv.writer(csvFile)
	total_usuarios=Usuario.objects.all().order_by('id')
	for usuario in total_usuarios:
		csvWriter.writerow([usuario.id,usuario.usuario,usuario.facultad.id])


def inicio(request):
	total_usuarios=Usuario.objects.all().order_by('id')
	total_facultades=Facultad.objects.all()
	list_user=Usuario.objects.filter(cant_seguidores__gt=0)

	for f in total_facultades:
		l_u=Usuario.objects.filter(cant_seguidores=5000,facultad=f)
		for u in l_u:
			print u.usuario+' '+u.facultad.nombre+' '+str(u.cant_seguidores)
	u_t=Usuario.objects.get(usuario='cparedesverduga')
	m_t=MentionUser.objects.filter(id_usuario_2=u_t.id)
	for m in m_t:
		u=Usuario.objects.get(id=m.id_usuario_1)
		print u.id
	list_esp=[53953,54170,54704,57665,54483,54048,54030,51432]
	for e in list_esp:
		u=Usuario.objects.get(id=e)
		print str(e)+' '+str(u.facultad.id)+' '+' '+u.facultad.nombre
	

	#tw=Tweet.objects.filter(usuario=u_t)
	#for t in tw:
	#	print t.comentario
	"""
	archivo = open('data2.tsv', 'a')
	archivo.write('letter	frequency	facultad\n')
	i=0
	for facultad in total_facultades:
		i+=1
		obj_usuario=Usuario.objects.filter(facultad=facultad)
		archivo.write(str(i)+'	'+str(len(obj_usuario))+'	'+str(facultad.siglas)+'\n')
	"""
	"""
	print 'usuario especial '+str(total_usuarios[len(total_usuarios)-1].id)
	list_mention=MentionUser.objects.all().order_by('id')
	to_mention=85717-1
	to_mention2=85717
	archivo = open('miserables.json', 'a')
	archivo.write('{\n')
	archivo.write('"nodes":[\n')
	for usuario in total_usuarios:
		archivo.write('{"name":"'+str(usuario.usuario)+'","group":'+str(usuario.facultad.id)+'},\n')
	archivo.write('],\n')

	archivo.write('"links":[\n')
	for mention in list_mention:
		archivo.write('{"source":'+str(int(to_mention)-int(mention.id_usuario_1))+',"target":'+str(int(to_mention2)-int(mention.id_usuario_2))+',"value":10},\n')
	archivo.write(']\n}')
	"""
	context={'total_usuarios':len(total_usuarios),'total_facultades':len(total_facultades)}
	
	return render_to_response('dataset/index.html',context, context_instance=RequestContext(request))

def tweetHora(hora,facultad):
	#datetime.datetime(int(anio),int(mes),int(dia),int(hora),int(minuto),int(segundo))
	cont=0
	try:
		facultad=Facultad.objects.get(siglas=facultad)
		lu=Usuario.objects.filter(facultad=facultad)
		for u in lu:
			lt=Tweet.objects.filter(usuario=u)
			for t in lt:
				if t.fecha.hour==hora:
					cont+=1
	except:
		pass
	return cont

def porcentajeFacultad(sigla):
	try:
		facultad=Facultad.objects.get(siglas=sigla)
		list_usuarios=Usuario.objects.all()
		usuario_facultad=Usuario.objects.filter(facultad=facultad)
		porcentaje=(100*len(usuario_facultad))/len(list_usuarios)
	except:
		porcentaje=0

	return porcentaje

def cantidadUsuario(sigla):
	try:
		facultad=Facultad.objects.get(siglas=sigla)
		usuario_facultad=Usuario.objects.filter(facultad=facultad)
		
		return int(len(usuario_facultad))
	except:
		return 0

def media():
	cnt_fcsh=cantidadUsuario('FCSH')
	cnt_fimcp=cantidadUsuario('FIMCP')
	cnt_fimcbor=cantidadUsuario('FIMCBOR')
	cnt_fict=cantidadUsuario('FICT')

	a=numpy.array([cnt_fcsh,cnt_fimcp,cnt_fimcbor,cnt_fict])
	return numpy.mean(a)

def varianza():
	cnt_fcsh=cantidadUsuario('FCSH')
	cnt_fimcp=cantidadUsuario('FIMCP')
	cnt_fimcbor=cantidadUsuario('FIMCBOR')
	cnt_fict=cantidadUsuario('FICT')

	a=numpy.array([cnt_fcsh,cnt_fimcp,cnt_fimcbor,cnt_fict])
	return numpy.var(a)

def desviaStandar():
	cnt_fcsh=cantidadUsuario('FCSH')
	cnt_fimcp=cantidadUsuario('FIMCP')
	cnt_fimcbor=cantidadUsuario('FIMCBOR')
	cnt_fict=cantidadUsuario('FICT')

	a=numpy.array([cnt_fcsh,cnt_fimcp,cnt_fimcbor,cnt_fict])
	return numpy.std(a)

def percentil50():
	cnt_fcsh=cantidadUsuario('FCSH')
	cnt_fimcp=cantidadUsuario('FIMCP')
	cnt_fimcbor=cantidadUsuario('FIMCBOR')
	cnt_fict=cantidadUsuario('FICT')

	a=numpy.array([cnt_fcsh,cnt_fimcp,cnt_fimcbor,cnt_fict])
	return numpy.percentile(a,50)



