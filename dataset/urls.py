from django.conf.urls import patterns, url

urlpatterns = patterns('dataset.views',
	 url(r'^/?$', 'inicio', name='inicio'),
	 url(r'^insertFacultades/?$', 'insertFacultades', name='insertFacultades'),
	 url(r'^insertUsuarios/?$', 'insertUsuarios', name='insertUsuarios'),
	 url(r'^deleteDobles/?$', 'deleteDobles', name='deleteDobles'),
	 url(r'^insertCantSeguidores/?$', 'insertCantSeguidores', name='insertCantSeguidores'),
	 url(r'^insertMention/?$', 'insertMention', name='insertMention'),
	 url(r'^deleteDobleMention/?$', 'deleteDobleMention', name='deleteDobleMention'),
	 url(r'^generarUsuariosCSV/?$', 'generarUsuariosCSV', name='generarUsuariosCSV'),
)