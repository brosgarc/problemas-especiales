from django.db import models
# Create your models here.
 		
class Facultad(models.Model):
	id=models.AutoField(primary_key=True)
	nombre=models.CharField(max_length=5050, verbose_name='Nombre')
	siglas=models.CharField(max_length=50, verbose_name='Siglas')	
		
class Usuario(models.Model):
	id=models.AutoField(primary_key=True)
	nombre= models.CharField(max_length=150, verbose_name='Nombre')
	usuario= models.CharField(max_length=150, verbose_name='Usuario')
	facultad= models.ForeignKey(Facultad)
	cant_seguidores=models.IntegerField(verbose_name='Seguidores')

class RelacionUser(models.Model):
	id=models.AutoField(primary_key=True)
	id_usuario_1=models.IntegerField(verbose_name='Usuario1')
	id_usuario_2=models.IntegerField(verbose_name='Usuario2')

class MentionUser(models.Model):
	id=models.AutoField(primary_key=True)	
	id_usuario_1=models.IntegerField(verbose_name='Usuario1')
	id_usuario_2=models.IntegerField(verbose_name='Usuario2')

class Tweet(models.Model):
	id=models.AutoField(primary_key=True)
	usuario=models.ForeignKey(Usuario)
	fecha=models.DateTimeField(auto_now=False)
	comentario=models.CharField(max_length=5000, verbose_name='Comentario')

